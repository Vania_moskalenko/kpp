
import java.io.*;

public class Main {
	
	
    public static String toTitleCase(String givenString) {
        String[] mass = givenString.split(" ");
        StringBuffer sB = new StringBuffer();

        
        for (int i = 0; i < mass.length; i++) {
            sB.append(Character.toUpperCase(mass[i].charAt(0)))
                    .append(mass[i].substring(1)).append(" ");
        }
        
        return sB.toString().trim();
    }
    
    

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("d:/text.txt"));
        String line = br.readLine();
        br.close();
        System.out.println(line);

        
        BufferedWriter bw = new BufferedWriter(new FileWriter("d:/text.txt"));
        String newLine = toTitleCase(line);
        System.out.println(newLine);
        bw.write(newLine);
        bw.close();
    }
}


import java.util.Scanner;

public class Human {
	
	String Name;
	String LastName;
	int Age;
	Scanner sc = new Scanner(System.in);
	
	
	void chooseName()
	{
		System.out.println("Input Name of Human: ");
		Name = sc.nextLine();
	}
	
	void chooseLastName()
	{
		System.out.println("Input Lastname of Human: ");
		LastName = sc.nextLine();
	}
	
	void chooseAge()
	{
		System.out.println("Input Age of Human: ");
		Age = sc.nextInt();
	}
	
	void createHuman()
	{
		chooseName();
		chooseLastName();
		chooseAge();
	}
	

}

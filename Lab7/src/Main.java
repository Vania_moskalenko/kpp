import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		int size = 3;
		int[][] mass = new int[size][size];
		Random rnd = new Random();
		int sum =0;
		int schetchik =0;
		int bound = 20;
		
		
		for(int i = 0;i<size;i++)
		{
			for(int j = 0;j<size;j++)
			{
				schetchik++;
				mass[i][j] =  rnd.nextInt(bound);
				if(schetchik%size==0)
					System.out.println(mass[i][j]);
				else
					System.out.print(mass[i][j]+" ");
			}
		}
		
		
		Thread Thr_1 = new Thread(new Runnable()
		{
			int sum =0;
			public void run() 
			{
				for(int i = 0;i<size;i++)
				{
					for(int j = 0;j<size;j++)
					{
						if(i==j)
						{
							sum += mass[i][j];
						}
						else if((i==0&&j==size-1) || (j==0&&i==size-1) )
						{
							sum += mass[i][j];
						}					 
					}
				}
					
				System.out.println("Summa diagonalnih elementov = "+sum);
				return ;
			}		
		});


		Thread Thr_2 = new Thread(new Runnable()
		{
			int dob = 0;
			int z=0;
			int [] dobmass = new int[size];
			public void run() 
			{
				for(int i = 0;i<size;i++)
				{
					for(int j = 1;j<size;j++)
					{
						if(i==0&&j==1)
							dob=mass[0][0];
							
						dob = dob*mass[j][i];
						
						if(j==size-1 && i!=size-1)
						{
							dobmass[z] = dob;
							dob=mass[0][i+1];
							z++;
						}
						else if(j==size-1 && i==size-1)
						{
							dobmass[z] = dob;
						}
					}
				}
				for(int i = 0;i<size;i++)
				{	
				System.out.println("Dobutok "+(i+1)+" stroki = "+dobmass[i]);
				}
				return ;
			}		
		});
		
		Thr_1.start();
		Thr_2.start();
		
		
	}

}


